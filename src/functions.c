#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_image.h>
#include "headers.h"

void greet(void)
{
  printf("Welcome to this game\n");

  return;
}

int initialize(void)
{
  int initstatus;
  initstatus = SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER);
  if (initstatus != 0)
  {
    printf("Error initializing SDL: %s\n", SDL_GetError());
  }

  return initstatus;
}

int makeWindow(int delay, char* imageLoc)
{
  // make the empty window
  SDL_Window* win = SDL_CreateWindow("Hi",
                                     SDL_WINDOWPOS_CENTERED,
                                     SDL_WINDOWPOS_CENTERED,
                                     640, 480, 0);
  if (!win)
  {
    printf("Error creating window: %s\n", SDL_GetError());
    SDL_Quit();
    exit(1);
  }
  
  // create renderer, which sets up graphics hardware
  Uint32 render_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
  SDL_Renderer* rend = SDL_CreateRenderer(win, -1, render_flags);
  if (!rend)
  {
    printf("Error creating renderer: %s\n", SDL_GetError());
    SDL_DestroyWindow(win);
    SDL_Quit();
    exit(1);
  }

  // load the image into memory
  SDL_Surface* surface = IMG_Load(imageLoc);
  if (!surface)
  {
    printf("Error creating surface: %s\n", SDL_GetError());
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);
    SDL_Quit();
    exit(1);
  }

  // load the image data into graphics hardware memory
  SDL_Texture* tex = SDL_CreateTextureFromSurface(rend, surface);
  SDL_FreeSurface(surface);
  if (!tex)
  {
    printf("Error creating texture: %s\n", SDL_GetError());
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);
    SDL_Quit();
    exit(1);
  } 

  // clear window, draw image to window
  SDL_RenderClear(rend);
  SDL_RenderCopy(rend, tex, NULL, NULL);
  SDL_RenderPresent(rend);

  // wait
  SDL_Delay(delay);
  
  // cleanup
  SDL_DestroyTexture(tex);
  SDL_DestroyRenderer(rend);
  SDL_DestroyWindow(win);

  return 0;
}

void quit(void)
{ 
  SDL_Quit(); 
}
 
