//#include <stdio.h>
//#include <SDL2/SDL.h>
#include "headers.h"
#define WINDOWTIMER 5000


int main()
{
  greet();

  // initialize
  if (initialize() != 0)
  {
    return 1;
  }
  char* imageLoc = "../images/icon.png";
  // create a window for a few seconds
  makeWindow(WINDOWTIMER, imageLoc);

  // clean up resources
  quit();

  return 0;
}

